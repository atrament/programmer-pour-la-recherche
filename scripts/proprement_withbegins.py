# ! /usr/bin/env python3
# coding: utf-8

"""Convertir des unités au système SI"""


import pint
import begin


@begin.start
def base_units(quantity):
    """Converts a quantity to base units"""
    _Q = pint.UnitRegistry()
    print(str(_Q(quantity).to_base_units()))
