#! /usr/bin/env python3


def count_divisors(x: "number to divide"):  #
    """
     returns number of divisors for numbers > 1
     """
    counter = 2
    for i in range(2, int(x ** 0.5 + 1)):
        if x % i == 0:
            counter += 2
    return counter
