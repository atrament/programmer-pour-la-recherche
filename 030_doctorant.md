# Le bon cadre de travail

Bien programmer, comme tout bon travail, est à la fois une question de méthode(s) et une question d'outil, de cadre de travail. Imaginez vous un atelier de menuisier sans la myriade d'outils chacun adaptés à une tâche précise\ ? Il en va de même pour la programmation, et ce chapitre a pour but de vous présenter quelques outils qui assistent le programmeur.

## Construire un environnement de travail

Un environnement de travail, en ce qui concerne la programmation, est l'ensemble des outils qui contribuent à assister le développeur dans la création de code. Cela va donc du système d'exploitation, s'il offre des bases saines pour améliorer la compatibilité, aux éditeurs et à l'auto complétion plus ou moins fournie qu'ils proposent, jusqu'au *linter* qui signalera les défauts du code produit au fil de l'écriture.

### Système d'exploitation:

Outre les avis partisans, les systèmes POSIX (c'est-à-dire, sauf exception, de la famille des Unix / Linux, ainsi que MacOS X) présentent de nombreux avantages pour programmer. L'un des plus notables est la *portabilité* ; toutes les versions de ces systèmes ayant suffisamment en commun pour permettre de programmer pour tous les systèmes.

Ils présentent également des *shells*[^shell] dont les fonctions facilitent le cycle de développement, et une capacité à "isoler" la création de programmes, évitant à un projet de perturber un autre, ou de dépendre de la configuration de la machine.

Enfin, compte-tenu d'une part de sa gratuité, et d'autre part de la pléthore d'outils libres destinés au développement, un système GNU/Linux constitue un choix avisé pour programmer[^dualboot].

[^shell]: un *shell* est l'interface en ligne de commande (souvent, on se l'imagine en blanc sur noir) proposée par un système.
[^dualboot]: Pour conserver un autre système (MS windows ® par exemple), il est possible d'installer Linux séparément, dans une configuration appelée "*dual boot*".

### Contrôle de version avec `git`

Cet outil est un moyen extrêmement robuste et puissant pour journaliser et garder les versions d'un projet de programmation. Son utilisation n'est pas instinctive pour tous, et son interface en ligne de commande semble souvent absconse aux débutants, mais la possibilité de travailler simultanément hors ligne sur le même projet, de revenir en arrière, et de séparer les axes de travail (par des "branches" git) en font l'outil de référence des développeurs depuis plus de 10 ans.

Un bon support d'apprentissage est [le livre *Pro Git* Par Scott Chacon][git-book]. Bien qu'il faille s'investir quelques heures pour pouvoir saisir les bases de git, le bénéfice de confort et de sécurité sur quelques semaines justifie cet effort !

Il se complète excellemment de l'outil [`git flow`][git-flow-1][][git-flow-2] pour assister le *workflow*.

[git-book]: https://git-scm.com/book/
[git-flow-1]: http://nvie.com/posts/a-successful-git-branching-model/
[git-flow-2]: https://github.com/nvie/gitflow

###   préparer un environnement de développement

Au sein de l'environnement de travail, l'environnement de développement est le cadre dans lequel le code est effectivement écrit, et celui dans lequel il est exécuté. Python propose un concept d'environnements virtuels, les *virtualenvs*, qui permettent de distinguer les cadres d'exécution (et de développement en général) de différentes projets.

L'un des moyens les plus simples pour utiliser ces environnements virtuels est [`pew`](https://github.com/berdario/pew), qui assiste l'utilisateur pour gérer ses environnements.

L'installation se fait^[sur une machine Linux, basée sur Debian (Debian, Ubuntu, LMDE, etc.)] avec les commandes suivantes :

~~~{.bash .numberLines}
sudo aptitude install python-pip # installer pip
sudo pip install pew # installer pew *via* pip
echo 'source $(pew shell_config) # pew auto completion'\
    >> ~/.bashrc  # ajouter l'autocompletion
echo "export WORKON_HOME='~/.virtualenvs' # pew home"\
    >> ~/.bashrc  # configuration de pew
~~~

En gardant une liste de modules python à jour dans un fichier `requirements.txt`^[c'est une convention du monde python pour la gestion des dépendances dans les paquets], `pew` peut directement créer un environnement de travail avec cette liste de paquets. Dans l'exemple suivant, la simple ligne `pew new -p python3 -r ./requirements.txt mon_env_de_demo` demande la création d'un environnement de travail avec les trois librairies listées dans `requirements.txt`.

~~~{.numberLines}
atrament@there ~/test
$ pew new -p python3 -r ./requirements.txt mon_env_de_demo
[...]
Installing collected packages: pint, begins, q
Successfully installed begins-0.9 pint-0.7.2 q-2.6
Launching subshell in virtual environment. Type 'exit' or 'Ctrl+D' to return.
mon_env_de_demo - atrament@there ~/test
$
~~~

On peut ensuite revenir à cet environnement virtuel python par la commande suivante:

~~~{.bash .numberLines}
atrament@there ~/test
$ pew workon mon_env_de_demo
Launching subshell in virtual environment. Type 'exit' or 'Ctrl+D' to return.
mon_env_de_demo - atrament@there ~/test
$
~~~

###   Utiliser un I.D.E.

Un IDE est l'outil principal de la programmation. Il s'agit d'un éditeur, et le plus simple d'entre eux permet d'écrire un programme fonctionnel. Mais un éditeur pourvu de fonctionnalités change le quotidien du programmeur ! L'éditeur parfait permet de taper plus vite, grâce notamment à l'auto completion : voici une liste des fonctionnalités que peuvent proposer les IDE existants.

Auto complétion :
:   au fil de la frappe, l'appui sur une touche (souvent `tab`) provoque l'affichage de différents choix correspondant à des variables définies dans le programme, parfois même dans les librairies importées. Cela évite les fautes de frappe et les erreurs de noms. Une bonne complétion automatique est souvent le premier critère de choix d'un IDE pour les développeurs expérimentés, et son importance explique pourquoi elle est ici présentée en premier.

Gestion des environnements virtuels:
:   Si un IDE supporte les `virtualenvs`, il permet de parcourir les librairies *du projet en cours* ar l'autocomplétion, et de lancer le programme dans cette configuration. Ainsi on évite les mauvaises surprises en livrant le code à un tiers (code dépendant de la machine sur laquelle il est lancé, de librairies installées gloabalement sur le système par exemple).

Inspection des données:
:   La possibilité d'examiner les variables pendant l'exécution du programme est un réel plus pour l'informatique scientifique : à l'exécution d'un programme, même sans avoir mis en place la visualisation des résultats, il devient possible de vérifier la cohérence des données dans les variables. et en cas de blocage, ou d'erreur, cela facilite considérablement le déboguage.

Execution par blocs:
:   si le programme en cours d'écriture est long à l'execution, comme c'est souvent le cas en recherche, pouvoir en exécuter une portion est très utile : on ne relance que la partie du code dont on veut vérifier la modification. C'est notamment très utile pour le prototypage, où l'on tâtonne pour trouver une solution à un problème.

Les IDE qui proposent ces fonctionnalités sont nombreux, on peut remarquer *Pycharm*, *Sublime*, ou encore *Spyder*, qui présentent chacun des qualités et des défauts les uns par rapport aux autres.

On peut également considérer le projet *Jupyter* : il fournit notamment le *Jupyter Notebook*, qui est un outil sans équivalent pour le prototypage.

Grâce à `pew` on peut tester cet outil simplement :

~~~{.numberLines}
atrament@there ~/test
$ pew mktmpenv -p python3 -i jupyter
[...]
42ce5ab265eabfea - atrament@there ~/test
$ jupyter notebook
~~~

Ici, on fait créer à `pew` un nouvel environnement virtuel temporaire jetable^[nommé "42ce5ab265eabfea", comme l'indique l'invite de commande], en y installant `jupyter`. La deuxième commande lance le *jupyter notebook* dans un navigateur.

### Configurer un *linter*

Un *linter* est un programme dont le rôle est d'analyser le code du projet en cours. Il en existe de nombreux, d'ailleurs les IDE listés précédemment proposent des *linters*, soit sous la forme de *plugins* soit nativement. `flake8` est un excellent linter pour python, qui en combine plusieurs autres pour analyser le code sur le style, la syntaxe et les variables orphelines (entre autres).

Il s'installe directement par pip au sein d'un virtualenv :

~~~{.bash .numberLines}
pip install flake8
# avec '--user' pip installera localement sans privilèges
flake8 mon_script_a_tester.py  # pour inspecter un script
~~~

Mais l'usage d'un linter externe est plus source de frustration que s'il peut être intégré à l'éditeur : c'est beaucoup moins contrariant de corriger son code au fur et à mesure que de se faire reprocher d'un coup des dizaines d'erreurs !

## Programmer "proprement"

À travailler avec des codeurs plus expérimentés, le jeune développeur s'entend (trop) souvent reprocher que son code n'est pas "propre". Mais qu'est-ce qui fait un code "propre"\ ? Voici quelques points de départ :

###   Conventions de nommage

Tous les langages de programmation un tant soit peu modernes différentient la "casse", c'est-à-dire la différence entre majuscule et minuscule. Ainsi `Calcul` et `calcul` peuvent très bien désigner deux objets différents !

Pour éviter à tout le monde de se tromper, limiter les hésitations, et pour avoir une cohérence entre développeurs, les langages et les projets suivent des conventions de nommage. Python se distingue par l'existence d'une [recommandation (la PEP 8)][pep8] pour l'ensemble du langage.

En (très) bref, cela permet de reconnaître directement les classes des fonctions, des variables, sans avoir à aller et venir au sein du code. Cela évite aussi les confusions et facilite l'utilisation ultérieure du code, qui sera plus vite compris par un développeur expérimenté.

*   une classe se désigne en `MotsCapitalises`. Ainsi un objet dont le nom commence par une majuscule est toujours une classe.
*   une variable globale est toujours `TOUTE_EN_MAJUSCULES`, les mots séparés par des *underscores* `_`. Elles ne devraient pas être déclarées dans la variable spéciale `__all__`^[nous laissons le soin au lecteur de se documenter par lui-même sur cette variable particulièrement utile pour éviter les pollutions entre modules].
*   les fonctions sont toujours `en_minuscules` les mots séparés par des *underscores* `_`.

Le reste des recommandations de la recommandation PEP 8 n'est pas aussi utile pour le développeur débutant, mais il est intéressant de la relire de temps à autres. Elle s'attache essentiellement à la lisibilité du code produit, et sur le volume de code, cela fait une réelle différence à la fois dans l'effort nécessaire à la lecture et par conséquent aussi au niveau de l'efficacité des développeurs.

[pep8]: https://www.python.org/dev/peps/pep-0008/

###   formatage du code

Connaître la PEP 8 dans son ensemble n'est pas nécessaire pour formatter correctement le code python : `flake8`, utilisé en module de votre IDE, vous corrigera au fur et à mesure de l'écriture du programme, contribuant à vous donner les bonnes habitudes.^[les plus rigoureux pourront même s'imposer des *git hooks* qui leur interdiraient de *commit* des scripts non conformes à la PEP8 !]

Il est aussi utile de limiter au maximum le recours aux variables globales. On peut pratiquement toujours les remplacer par des arguments en ligne de commande, ce qui présente le double avantage de $1)$ ne pas exiger de l'utilisateur qu'il altère le code du programme pour le faire fonctionner, $2)$ lancer efficacement plusieurs configurations à partir d'une même version du programme.

Un excellent point de départ pour ce faire est la librairie [`begins`](http://begins.readthedocs.io/). Voici un code préparé avec des variables globales, que nous allons convertir à l'aide de begins.

~~~{.python .numberLines}
IN = '2km'

import pint

_Q = pint.UnitRegistry()

print(str(_Q(IN).to_base_units()))
~~~

Ce script simpliste utilise la librairie `pint` pour convertir la grandeur définie par la variable globale `IN`, et l'affiche en sortie. Pour convertir n'importe quelle autre valeur, il faut éditer le script avant de le relancer !^[Avant de se moquer de l'absurdité d'une telle démarche, il faut avoir vu quelques codes de laboratoire. Ce genre de construction est bien plus fréquent qu'on ne le pense.]

Changeons ce script pour qu'il puisse simplement servir à de nombreuses conversions. Au passage, nous allons le convertir pour respecter les bonnes pratiques.

~~~{.python .numberLines}
#! /usr/bin/env python3

"""Convertir des unités au système SI"""

import pint
import begin


@begin.start
def base_units(quantity):
    """Converts a quantity to base units"""
    _Q = pint.UnitRegistry()
    print(str(_Q(quantity).to_base_units()))
~~~
Voici un détail ligne à ligne de ce script :

1:
:   cette déclaration permet, pour les systèmes POSIX, de déclarer que ce fichier s'exécute avec python 3, ce qui permettra de le lancer par lui-même une fois rendu executable.

3:
:   cette chaîne de caractères est une *"docstring"*, elle sert de documentation basique pour ce fichier.

4:
:   Une ligne vide sépare l'en-tête des imports.

5 et 6:
:   Autant que faire se peut, on évite la construction `from module import *`. L'import de modules entiers n'est pas forcément coûteux (en performance), mais il améliore la lisibilité.


7 et 8:
:   Deux lignes vides précèdent une déclaration de fonction.

9:
:   cette construction syntaxique propre à python est un "décorateur de fonction". Inutile d'entrer dans les détails dans le cadre de ce document.

10 et suivantes:
:   la procédure de traitement est tranférée à la fonction `base_units`, sa *docstring* servira de message de documentation en ligne de commande.

Voici le résultat en ligne de commande :

~~~{.bash .numberLines}
$ python script.py -h  # un message d'aide !
usage: script.py [-h] QUANTITY

Converts a quantity to base units

positional arguments:
  QUANTITY

optional arguments:
  -h, --help  show this help message and exit
$ python script.py 2km # la valeur de départ
2000.0 meter
$ python script.py '15m*15m'  # grâce à la flexibilité de pint
225 meter ** 2
$ python script.py '50kph' # 'kph' pour kilometres/heure
13.88888888888889 meter / second
$ python script.py '50 kph * 20 minute'  # calculs simples
16666.666666666668 meter
~~~

En combinant effectivement des librairies existantes, non seulement on peut se libérer de la peine de reproduire un travail existant, mais on peut aussi, comme l'illustre l'exemple précédent, gagner en souplesse d'utilisation, à la fois pour l'utilisateur et pour les utilisation au sein de projets plus complexes. Des exemples de librairies^[on parle souvent en python de *modules* ou de *packages* plutôt que de "librairies". La nuance est extrêmement ténue] utiles sont données dans [@20libs]^[téléchargeable gratuitement à l'heure où ces lignes sont écrites].

Pour résumer ; un code "propre" est lisible, simplement structuré, peut être importé ou utilisé sans modification même minime.

### "Programmer" est une compétences qui s'entraîne.

En commençant de programmer, il faut prendre conscience qu'il s'agit d'une compétence. Celle-ci se valorise, si elle est utile à votre travail, et peut valoir des sous sur la fiche de salaire. Cela dit, pour être effectivement valorisée, il faut l'exercer, la pratiquer pour garder un niveau suffisant.

Non seulement il faut revenir sur du code régulièrement, pour continuer de pratiquer, mais y consacrer une attention et une concentration particulière.
Pas de miracle possible : pour être un bon programmeur, il faut s'investir, en temps et intellectuellement.

Il faut aussi prendre l'habitude de se documenter, de se tenir au courant des nouvelles techniques et technologies. Pour une même tâche à faire accomplir à un programme, la façon la meilleure évolue avec le temps, et selon les critères de sélection de "la meilleure". Il peut être apparue une solution mature d'abstraction plus facile à lire, ou bien un paradigme de programmation plus efficace, ou encore plus extensible... Voire les trois à la fois !



