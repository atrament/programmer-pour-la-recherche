## Travail à distance

### Fonctionnement général

Un dépôt git est une copie complète et autonome. Il peut vivre une vie heureuse sur notre disque dur, comme ce fut le cas jusqu'alors.

L'intérêt de `git` pour le travail distribué est sa facilité à créer des branches, et que ce travail isolé permette de manipuler l'arbre sans même de lien réseau avec un dépôt en ligne. C'est idéal pour travailler en déplacement, et ensuite partager ses contributions avec le dépôt central.

Un point de technique, pour commencer : comme chaque dépôt (y compris chaque copie locale) est un "*clone*" autonome, beaucoup de *workflows* différents existent, et on peut tout imaginer, en partant du principe que chaque dépôt est une "famille de branches". Nous verrons plus loin que les dépôts peuvent interagir de façon plus ou moins complexe.

À moins que le projet soit colossal, un seul dépôt centralisé, et une gestion méthodique des branches communes suffisent à travailler sereinement. Ce dépôt se référence comme une "`remote`"^[un dépôt distant] pour notre instance de `git`. À partir de là, il faudra de temps à autre se synchroniser avec la remote en question.

### `push`, `pull`, et `fetch`

Les opérations de synchronisation offertes par `git` sont au nombre de trois :

-   `git push` : pour envoyer, ou *pousser* des références (tags, branches) vers une remote, avec les objets associés (i.e. les commits et les données qu'ils représentent).
-   `git pull` : pour récupérer et appliquer les changements à une branche. C'est grossièrement l'équivalent d'une instruction `git merge` entre notre dépôt et une remote.
-   `git fetch` : pour récupérer les références, objets et changements dans une remote mais sans les appliquer immédiatement à la branche courante.

La remote principale d'un projet est par convention nommée `origin` (car on suppose que le projet est un clone de `origin`). Nous allons utiliser ce nom par la suite.

#### Créer un dépôt

Il existe de nombreux moyens pour créer un dépôt : [github](https://github.com), [gitlab](https:gitlab.com), ou [framagit](http://framagit.org) proposent par exemple gratuitement d'héberger nos dépôts `git` en ligne. Si le besoin s'en fait sentir, on peut également créer notre dépôt `git` sur une machine du réseau, sans presque de configuration qu'une connexion ssh fonctionnelle.

Ce n'est pas plus simple que de créer un compte sur un des services précédents, mais c'est bien plus simple qu'il n'y paraît. Et comme de toutes façons les sites en ligne expliquent très bien comment créer et cloner des dépôts chez eux, autant créer notre dépôt privé.

#### créer un clone "nu"

Un serveur `git` utiliser un dépôt nu. Ceux-ci ont, par convention, un nom qui se termine en `.git`. Cloner un dépôt en un dépôt nu se fait avec `git clone` comme suit.

    $ git clone --bare my_project my_project.git

Cela crée un dossier `my_project.git` qui contient les données du projet. La sortie de cette opération est un peu surprenante, à cause des rouages internes de `git`.

#### Préparer le serveur

Il faut un accès ssh au serveur pour y copier le dépôt nu avec `scp`^["***s**sh **c**o**p**y*"].

    $ scp -r my_project.git user@git.example.com:/opt/git

Notons qu'il est aussi très pratique de créer un utilisateur `git@git.example.com` et de placer les dépôts dans son dossier `home`, l'adresse du dépôt est alors plus lisible en `git@git.example.com:my_project.git`.

À ce stade, les utilisateurs ayant accès au dossier contenant le dépôt peuvent cloner le dépôt avec `git clone user@git.example.com:/opt/git/my_project.git` ou avec `git clone git@git.example.com:my_project.git`.

Les utilisateurs avec les droits d'écriture auront la possibilité de *pousser* vers ce dépôt. Git adaptera les permissions automatiquement avec les commandes suivantes :

    $ ssh user@git.example.com
    $ cd /opt/git/my_project.git
    $ git init --bare --shared

À ce stade, le dépôt est totalement fonctionnel. *Pro Git* [@progit, chap."*Getting Git on a Server*" ] propose d'autres possibilités et des configurations de confort supplémentaires.

Notons que ce qui précède est littéralement le nécessaire pour monter un serveur `git` opérationnel : créez simplement des comptes utilisateurs avec un access ssh et vous pouvez collaborer sur un même projet immédiatement.
Cette installation peut convenir pour des besoins de gestion de droits "basiques" et jusqu'à quelques dizaines d'utilisateurs.^[Au delà, il sera sans doute utile de passer à un autre outil comme `Gitolite` pour faciliter l'administration du dépôt., voire une installation personnelle de `Gitlab`]

Je dispose d'un serveur nommé `papaye`, sur lequel je vais créer une copie du dépôt de test utilisé dans le tutoriel précédent.

    $ pwd
    $ cd .. && git clone --bare test test.git
    Clonage dans le dépôt nu 'test.git'
    fait.
    $ scp -r test.git/ papaye:test.git  # vers /home/<moi>/test.git
    description                                                     100%   73    28.2KB/s   00:00
    HEAD                                                            100%   20     6.3KB/s   00:00
    ec6ace484d6cd92510fbc9c95                          100%  172    59.4KB/s   00:00
    [...]
    exclude                                                         100%  240   100.4KB/s   00:00
    packed-refs                                                     100%  223    98.3KB/s   00:00
    config                                                          100%  111    51.1KB/s   00:00
    $ rm -rf test.git

Il ne reste qu'à déclarer dans le dépôt existant cette nouvelle remote :

    $ cd test
    $ git remote add origin papaye:test.git

Le dépôt est prêt, nous pouvons continuer notre découverte des remotes !

#### `git fetch` : prendre des nouvelles du serveur

Avec `git fetch`, notre dépôt va synchroniser les références depuis le serveur. Essayons de suite de lister les branches :

    $ git branch --all  # voir toutes less branches
    * dev
      generer_fichiers
      master
    $ git fetch  # se renseigner sur l'état du serveur
    Depuis papaye:test
     * [nouvelle branche] dev              -> origin/dev
     * [nouvelle branche] generer_fichiers -> origin/generer_fichiers
     * [nouvelle branche] master           -> origin/master
    $ git branch --all
    * dev
      generer_fichiers
      master
      remotes/origin/dev
      remotes/origin/generer_fichiers
      remotes/origin/master

Les branches distantes sont apparues sous `remotes/origin`. Elles apparaissent en rouge sur certaines versions de `git`, car nous n'en avons pas les copies locales. Nous le voyons sur l'arbre d'après `gitk` (@fig:remote01).

![`gitk` référence les branches distantes après `git fetch` (une opération `git rebase` a été faite auparavant, ce qui explique la branche `generer_fichiers`)](img/remotes_fetch_01.png){#fig:remote01}

Créons un deuxième clone de ce dépôt, pour simuler le travail d'un autre collaborateur.

    $ cd ..  # remonter d'un répertoire
    $ git clone papaye:test.git test_michel
    Clonage dans 'test_michel'...
    remote: Counting objects: 161, done.
    remote: Compressing objects: 100% (125/125), done.
    remote: Total 161 (delta 41), reused 0 (delta 0)
    Réception d'objets: 100% (161/161), 14.46 KiB | 0 bytes/s, fait.
    Résolution des deltas: 100% (41/41), fait.
    $ cd test_michel

Nous voici dans un nouveau dossier, copie conforme de la remote. Nous allons imaginer que ce deuxième collaborateur se nomme Michel, et configurer son dépôt pour que ses commits se distinguent des précédents :

    $ git config --local user.name "Michel Michel"
    $ git config --local user.email "michel@mon_labo.org"

Michel se trouve sur la branche `dev` (qui était la branche active lors du clonage de la remote), comme l'indique un `git status` dans son dépôt (`test_michel`):

    $ git status
    Sur la branche dev
    Votre branche est à jour avec 'origin/dev'.
    rien à valider, la copie de travail est propre

Une indication supplémentaire nous est donnée : la branche `dev` locale est "à jour" avec la branche `dev` sur `origin`. Il faut comprendre "à jour avec *la dernière version connue* de `origin/dev`, c'est  à dire le dernier `git fetch`.

Faisons, toujours au nom de Michel, quelques nouveaux commits de données :

    $ mkdir -p data/2016-xx-xx_simu_michel
    $ git checkout -b data_sim_michel
    $ for fichier in sim_data_{01..05}.txt ; do
        echo "données de $fich" > data/2016-xx-xx_simu_michel//${fichier} ;
        git add data/2016-xx-xx_simu_michel/${fichier} ;
        git commit -m "ajoute ${fichier} aux données simulées de Michel" ;
    done

    [dev 1898c43] ajoute sim_data_01.txt aux données simulées de Michel
     1 file changed, 1 insertion(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_01.txt
    [dev f3fbcf9] ajoute sim_data_02.txt aux données simulées de Michel
     1 file changed, 1 insertion(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_02.txt
    [dev 05d3928] ajoute sim_data_03.txt aux données simulées de Michel
     1 file changed, 1 insertion(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_03.txt
    [dev 16e85b2] ajoute sim_data_04.txt aux données simulées de Michel
     1 file changed, 1 insertion(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_04.txt
    [dev 13a83c5] ajoute sim_data_05.txt aux données simulées de Michel
     1 file changed, 1 insertion(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_05.txt

#### `git push` : Share your labor of love.

Ces commits n'existent que dans le dépôt de Michel , puisque il n'a rien entreprit pour les publier au monde^[enfin, à `origin`, mais ce serait déjà mieux que rien...].

Michel dispose de deux choix pertinents :

-   Il estime que sa branche est mature et mérite d'être fusionné à `dev` ***puis*** que les mises à jour soient envoyées à `origin`
-   Il estime que sa branche doit être sauvegardée en l'état, et qu'un autre collaborateur vérifie sa contribution avant de pratiquer la fusion.

Le premier choix est trivial (`git checkout dev ; git pull ; git merge --no-ff data_sim_michel ; git push`), nous allons explorer la seconde possibilité, qui reflète par ailleurs ce qui se pratique sur les hébergements de type `Gitlab` ou `github` où les fusions peuvent être faites par une interface web.

Assurons nous que Michel soit bien sur la branche `data_sim_michel`:

    $ git branch
    * data_sim_michel
      dev

Essayons de pousser la branche courante :

    $ git push
    fatal: La branche courante data_sim_michel n'a pas de branche amont.
    Pour pousser la branche courante et définir la distante comme amont, utilisez

        git push --set-upstream origin data_sim_michel

Réessayons avec la commande suggérée, qui crée et configure la branche sur `origin` :

    $ git push --set-upstream origin data_sim_michel
    Décompte des objets: 21, fait.
    Delta compression using up to 4 threads.
    Compression des objets: 100% (14/14), fait.
    Écriture des objets: 100% (21/21), 1.75 KiB | 0 bytes/s, fait.
    Total 21 (delta 3), reused 0 (delta 0)
    To papaye:test.git
     * [new branch]      data_sim_michel -> data_sim_michel
    La branche data_sim_michel est paramétrée pour suivre la branche distante data_sim_michel depuis origin.

Voici que `data_sim_michel` est sauvegardée sur le dépôt distant. Mais comment les autres collaborateurs y accèdent ? Reprenons notre premier dépôt, qui n'a pas (encore) connaissance des contributions de Michel:

    $ cd .. ; cd test  # retourner au dépôt de "moi-même"
    $ git fetch  # Prendre des nouvelles de 'origin'
    remote: Counting objects: 22, done.
    remote: Compressing objects: 100% (14/14), done.
    remote: Total 21 (delta 3), reused 0 (delta 0)
    Dépaquetage des objets: 100% (21/21), fait.
    Depuis papaye:test
     * [nouvelle branche] data_sim_michel -> origin/data_sim_michel
    $ git checkout origin/data_sim_michel  # cela peut-il marcher ?
    Note : extraction de 'origin/data_sim_michel'.

    Vous êtes dans l'état « HEAD détachée ». Vous pouvez visiter, faire des modifications
    expérimentales et les valider. Il vous suffit de faire une autre extraction pour
    abandonner les commits que vous faites dans cet état sans impacter les autres branches

    Si vous voulez créer une nouvelle branche pour conserver les commits que vous créez,
    il vous suffit d'utiliser « checkout -b » (maintenant ou plus tard) comme ceci :

      git checkout -b <nom-de-la-nouvelle-branche>

    HEAD est maintenant sur 09f17e8... ajoute sim_data_05.txt aux données simulées de Michel

Le long message informatif de `git` est causé par l'absence de référence locale : nous parcourons le dépôt commit par commit, mais sans le "fil d'Ariane" assuré par une branche. Créons plutôt notre copie locale de la branche distante : En réalité il s'agit de la même commande (`git checkout` -- basculer sur une branche), à qui l'on va demander de créer une copie locale (avec `-b <nom_local>`).

    $ git checkout dev
    Basculement sur la branche 'dev'
    $ git checkout -b data_sim_michel origin/data_sim_michel
    La branche data_sim_michel est paramétrée pour suivre la branche distante data_sim_michel depuis origin.
    Basculement sur la nouvelle branche 'data_sim_michel'

"Moi-même" inspecte les commits, et décide de les fusionner à dev :

    $ git checkout dev
    Basculement sur la branche 'dev'
    $ git merge --no-ff data_sim_michel -m "Fusionne les données validées de Michel."
    Merge made by the 'recursive' strategy.
     data/2016-xx-xx_simu_michel/sim_data_01.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_02.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_03.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_04.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_05.txt | 1 +
     5 files changed, 5 insertions(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_01.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_02.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_03.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_04.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_05.txt

Pour que Michel (ou quiconque) puisse reprendre la branche `dev` mise à jour, "Moi-même" n'a plus qu'à utiliser `git push`

    $ git branch  # est-on bien sur dev ?
      data_sim_michel
    * dev
      generer_fichiers
      master
    $ git push --set-upstream origin dev  # premier push, configurer au passage
    Décompte des objets: 1, fait.
    Écriture des objets: 100% (1/1), 243 bytes | 0 bytes/s, fait.
    Total 1 (delta 0), reused 0 (delta 0)
    To papaye:test.git
       58e0289..77edd04  dev -> dev
    La branche dev est paramétrée pour suivre la branche distante dev depuis origin.

Voici l'état de nos références, vu dans `gitk` (@fig:remotes_fetch_02).

![références après la fusion et le `push` de `dev`](img/remotes_fetch_02.png){#fig:remotes_fetch_02}

La branche dédiée au travail de Michel étant entièrement fusionnée, quiconque peut la supprimer de `origin` comme de son dépôt local sans risque : Faisons le faire à Michel.

    $ git push origin :data_sim_michel
    To papaye:test.git
     - [deleted]         data_sim_michel

Cette syntaxe quelque peu obscure se base sur la syntaxe non abrégée de `git push` : `git push <remote> branche_locale>:<banche_distante>`. En n'utilisant rien en guise de branche locale, elle efface la branche distante.

Reste à Michel à effacer sa version locale de la branche : Il procède à un `fetch`, prévoyant ensuite de supprimer sa référence à `data_sim_michel` avec la commande `git branch -d data_sim_michel`.

    $ git fetch
    remote: Counting objects: 1, done.
    remote: Total 1 (delta 0), reused 0 (delta 0)
    Dépaquetage des objets: 100% (1/1), fait.
    Depuis papaye:test
       58e0289..77edd04  dev        -> origin/dev

`dev` a avancé, et `git fetch` en informe Michel. Avec `git checkout dev` il se rend sur cette branche.

    $ git checkout dev
    Basculement sur la branche 'dev'
    Votre branche est en retard sur 'origin/dev' de 6 commits, et peut être mise à jour en avance rapide.
      (utilisez "git pull" pour mettre à jour votre branche locale)

#### `git pull` : appliquer les changements d'autrui.

La branche `dev` sur origin est en avance (il ya des commits sur origin qui n'existent pas dans notre dépôt). `git` n'applique pas les changements de lui-même (cela évite d'appliquer des changements indésirables). Pour "tirer" les changements de `origin/dev`, on utilise `git pull`

    $ git pull
    Mise à jour 58e0289..77edd04
    Fast-forward
     data/2016-xx-xx_simu_michel/sim_data_01.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_02.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_03.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_04.txt | 1 +
     data/2016-xx-xx_simu_michel/sim_data_05.txt | 1 +
     5 files changed, 5 insertions(+)
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_01.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_02.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_03.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_04.txt
     create mode 100644 data/2016-xx-xx_simu_michel/sim_data_05.txt

Comme la sortie de cette commande le laisse supposer, `git pull` est l'équivalent d'un `git fetch` suivi d'un `git merge`. Michel est donc à jour et peut reprendre son travail.

#### Astuces

Pour suivre plus facilement l'évolution de l'arbre, Il est possible d'ajouter des informations à la ligne de commandes Linux, en éditant le fichier `~/.bashrc`.
La manipulation n'est pas très difficile, mais elle est un peu technique. [@progit, chap. "Tips and Tricks"] indique comment préparer la complétion automatique.
Celle-ci définit `__git_ps1` qui est utile pour changer l'invite de `bash`.

~~~ {.bash}
# extrait de ~/.bashrc
source /etc/bash_completion
__my_mk_prompt () {
    export PS1="\u@\h \w `__git_ps1`\n\$ "
}
PROMPT_COMMAND="__my_mk_prompt"
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWUPSTREAM="auto"
~~~~~~

Ce qui ajout à la fin de l'invite de `bash` un court descriptif de l'état du dépôt local.

### `git rebase` : reprendre le train en marche

Partons du dépôt suivant dont le log est présenté en @fig:rebase_01.

![Projet factice avant `rebase`](img/rebase_01.png){#fig:rebase_01}

Si l'on veut `merge` l'une des branches `eden` ou `homo` sur `master`, l'arbre des commits sera moins lisible, avec des branches qui se croisent et se doublent.

Cela pose en plus le risque de conflits de fusion qu'il faudrait corriger sur `master`, alors qu'on préfèrerait pouvoir prendre en compte les évolutions de `master` *avant* la fusion, dans notre branche.

En fait, tout ceci serait bien plus simple si `homo` ne divergeait pas au niveau du commit `82d2714` mais au niveau du commit `7ebe655`, le plus récent de `master`. Et si `eden` ne divergeait pas au niveau de `c230bf7`, mais *après la fusion de `homo`*.

`git rebase` est exactement destiné à cela. Il remonte l'historique jusqu'à un ancêtre commun, puis vers le nouveau point de départ de la branche, et "rejoue" les commits de la branche à partir de là.

    $ git checkout homo
    Basculement sur la branche 'homo'
    $ git rebase master
    Premièrement, rembobinons head pour rejouer votre travail par-dessus...
    Application de  recoit un souffle de vie

On obtient l'arbre présenté en @fig:rebase_02:

![après rebase de homo sur master](img/rebase_02.png){#fig:rebase_02}

Et on peut simplement exécuter `git checkout master ; git merge --no-ff homo` pour obtenir @fig:rebase_03:

![Branche `homo` fusionnée sans croisement de branche](img/rebase_03.png){#fig:rebase_03}

Faisons de même pour la branche `eden` :

    $ git checkout eden
    Basculement sur la branche 'eden'
    $ git rebase master
    Premièrement, rembobinons head pour rejouer votre travail par-dessus...
    Application de  mûrit une pomme
    $ git checkout master
    Basculement sur la branche 'master'
    $ git merge --no-ff eden
    Merge made by the 'recursive' strategy.
     jardin.txt | 0
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 jardin.txt

Chacune des branche est bien lisiblement distincte des autres, et consiste en commits successifs facilement repérables dans le log (@fig:rebase_04).

![log des commits après les manipulations](img/rebase_04.png){#fig:rebase_04}

Il peut être tentant d'abuser de `rebase`, pour faire un arbre plus agréable à l'œil. Cependant, `git rebase` n'est pas toujours pertinent. Il convient surtout pour :

-   les branches éphémères, quand `dev` a un peu avancé entre leur début et leur fin,
-   les très vielles branches abandonnées, pour lesquelles il vaut mieux gérer les éventuels conflits avant la fusion,
-   les commits qui n'ont jamais été `push` et dont personne ne dépend.

Ce dernier point est un peu surprenant : En réalité, les commits de la nouvelle branche ne sont pas les même, ce sont des nouveaux commits que `git` "reconstitue" pour nous. Si nous n'avons jamais partagé ce travail par `push`, ou que nous reprenons une branche sur laquelle plus personne ne se base, ce n'est pas un problème. Mais si un commit a pour parent l'un de ceux que l'on `rebase`, toute sa généalogie sera potentiellement corrompue\ !

## FAQ de `git`

### Pourquoi utiliser `git` ? Je ne crois pas en avoir besoin.
En écrivant ce document, j'ai utilisé (pour le moment) une quarantaine de commits. Je suis revenu aujourd'hui au moins deux fois sur mes pas avec `git reset -- <fichier>` pour retrouver des fichiers effacés par erreur. J'ai pourtant passé seulement quelques minutes de ma journée à invoquer `git`, alors qu'il m'a fait gagner plusieurs heures de travail rien qu'aujourd'hui.

Si ce n'était que pour la possibilité d'avoir un contrôle efficace sur les versions, cela serait déjà un avantage d'utiliser `git`^[ce qui justifie de l'utiliser pour toute une variété d'activités]. La possibilité de travailler sur un sujet sans être influencé par les actions d'autres en est une deuxième bonne raison. La facilité avec laquelle on peut mettre en commun son travail est également une bonne raison. Dans le contexte de la programmation en recherche, la possibilité de remonter le fil du projet et d'en connaître l'histoire en détail est d'une aide précieuse lors des réunions de mise en commun, des reviews, etc.

### Il y a un bug dans mon code, mais je ne sais pas où !
Sans entrer dans le détail de son utilisation, préparez une manipulation qui mette en lumière ce bug, puis utilisez `git bisect`. Il s'agit d'un outil de `git` qui parcourt l'arborescence des commits, et vous permettra de trouver le commit qui a introduit le bug, donc de rapidement en isoler la cause. Il faut bien sûr que le bug ait été absent sur au moins une version !

### Ce code est affreux, qui a pu écrire cela ?
Essayez `git blame <cefichier>`, qui donne le commit source et l'auteur pour chaque ligne. Il est également intégré à certains éditeurs de texte, et à `gitk`, ce qui le rend plus lisible.

### J'ai fait trop de modifications, je voudrais faire plusieurs commits avec ces changements.
Au lieu d'utiliser `git add <monfichier>` ou `git add *` ou encore `git commit -a`^[qui enregistrent tous beaucoup de changements d'un coup, et donc font des commits moins lisibles gar moins spécifiques], essayez de passer par `git add --patch`. Il vous présentera, de façon interactive, des blocs de fichiers modifiés. Il peut être très utile de consulter `git diff` voire de le garder à vue pendant la manipulation. Cette fonction est surtout utile quand on a oublié de commit depuis longtemps et qu'on veut garder une bonne visibilité des changements individuels. N'oubliez pas non plus que créer une branche ne change pas l'état du dossier, il est donc tout à fait possible de créer une branche pour essayer, et de revenir en arrière en abandonnant les commits ensuite pour recommencer différemment.

### Faut il `git merge`, ou `git merge --no-ff` ?
Dans l'immense majorité des cas, s'il faut pouvoir examiner le travail accompli, il est préférable d'utiliser `--no-ff`, qui produit un arbre moins élégant, mais beaucoup plus lisible par la suite.

### Je reprends une branche abandonnée depuis longtemps, je vais avoir des conflits de fusion par dessus la tête !
C'est sûrement le bon moment pour se pencher sur `git rebase` ! En greffant cette "vielle" branche à un commit récent (et pertinent), vous n'éviterez pas les problèmes, mais avec un peu de prudence et en vérifiant bien le fonctionnement du code dès le début, vous pourrez repartir sur une base saine, plutôt que de devoir tout reprendre à la fusion.

### On m'a dit de `squash` mes commits, qu'est-ce ?
Une commande avancée de `git` est `git rebase`, qui permet de modifier une série de commits existant pour changer la branche source.
Par exemple, cela permet de baser une branche issue de `master` pour qu'elle prenne son origine sur `dev`.
Elle est utile notamment pour faire artificiellement "repartir" une branche depuis la tête courante de `dev` (typiquement après un `git pull`).
On peut utiliser `rebase` pour réordonner et cumuler les commits de la branche, ou écraser deux commits en un "*squash*".
À moins d'être très sûr de ce qu'on fait, la complexité et le risque de la manipulation ne justifie pas la manipulation `squash`.

### Est-ce que ça vaut la peine de faire une branche ?
Oui. Créer une branche est très léger avec `git`, cela présente en plus l'avantage de très rapidement distinguer le fil de votre travail d'un autre. Au pire, il sera toujours possible en cas de souci d'abandonner cette nouvelle branche.

### J'ai fait une faute de frappe dans mon message de commit.
On peut corriger un commit **avant d'en faire un nouveau** avec `git commit --amend`. Cela permet de corriger un message mal formulé, ou avec une faute de frappe. Attention toutefois, il ne faut jamais corriger un commit partagé vers une *remote*.

### Ça fait quelle taille, un commit, en principe ?
Il n'y a pas de règle. Enfin, pas en terme de nombre de lignes. Par contre, il est à peu près unanimement convenu qu'un commit doit représenter *un changement atomique* dans le code ou le travail. En clair, on doit pouvoir appliquer un commit sans devoir absolument en appliquer un autre pour que ce soit correct (pas d'erreur de syntaxe par exemple).
Et l'on ne doit pas avoir de sujets disjoints dans un même commit.
Pour illustrer cette notion un peu floue, dans ce document, chaque question de la FAQ représente un commit, chaque paragraphe également, chaque création de chapitre ou de fichier donne lieu à un commit également.

### C'est le bazar dans notre arbre de commits, comment peut on réarranger les commits ?
On ne peut pas. Enfin, si, on peut, en jouant sur `rebase` et le squash de commits, et en bricolant. Mais il ne faut pas. D'une part, la généalogie des commits ne sera plus fiable, et si du travail se base sur cette généalogie, cela le mettrait en péril.
D'autre part, l'esthétique de l'arbre n'a pas vraiment d'importance : si votre workflow *actuel* est fiable, alors le fait que le projet ait eu une phase chaotique n'a rien de choquant, c'est même très courant.

Par contre, si chaque nouvel arrivant fiche le bazar systématiquement, c'est sûrement signe que votre workflow n'est pas fiable, ou bien qu'il n'est pas correctement documenté. Jetez un œil à votre `contributing.txt`, pour voir ?

### Le message de commit, c'est pénible à écrire.
Ce n'est pas une question. Cela dit, c'est souvent une remarque qu'on entend. Ce qui, ironiquement, est le plus cocasse, c'est que les nouveaux venus sur git cherchent à copier leurs habitudes d'avant, quand ils n'utilisaient pas de système de contrôle de version.
On voit donc des messages de commits qui précisent la date, ou l'auteur, voire qui mentionnent tout autre chose^["Il est tard", "ça c'est fait", "prêt à livrer" ne sont **pas** des messages de commit adaptés.].
Un message de commit doit être court, explicite, reconnaissable. Un bon moyen est de commencer par un verbe d'action, et d'un minimum de précisions, on lira alors comme "ce commit..." : "ajoute les données de Michel, expérience de Marseille", ou "corrige `fonction_boiteuse` (fuite mémoire)" ou encore "Ajoute un chapitre sur `git`" sont acceptables. En cas de besoin, le message peut s'étendre sur plusieurs lignes. La convention est de laisser la deuxième ligne vierge comme pour séparer le message des détails, et de rester assez concis et factuel.

En utilisant à bon escient le message de commit, il devient assez facile pour les collaborateurs de suivre les évolutions d'une branche, voire de comprendre ce qui a été apporté par une fusion de branche.

Les tags sont par contre destinés à étiqueter d'autres informations et à repérer des commits particuliers. Bien sûr, le tag de version "`v1.3`" est utile, mais on peut être plus créatif, et marquer des jalons par exemple.^[Notez que `git describe` vous propose un identifiant de version basé sur le dernier tag visible.]

### Notre projet regroupe plusieurs sous projets indépendant : Comment utiliser des sources d'un autre dépôt comme ressources dans notre projet ?

Cela dépasse le cadre de ce document, mais `git submodule` permet d'inclure des dépôts git les uns dans les autres. Référez vous à [@progit, chap. "submodules"] pour plus de détails.
C'est particulièrement utile pour les librairies qu'utilise votre projet, ou pour affiner la gestion de portions du projet.

### Je ne peux plus changer de branche !

Vous êtes en cours de travail et avez des changements non indexés. Git ne vous laissera pas perdre ce travail avant de `checkout` ou `merge`. Soit il faut créer une branche et faire un commit qu'on corrigera avec `--amend` une fois revenu (c'est le mieux, à condition de faire un message de commit explicite comme "W.I.P. : en cours de résolution du problème de natalité dans les maisons de retraite"). Soit on peut mettre ça de côté quelques minutes dans un [`git stash`](http://sametmax.com/soyez-relax-faites-vous-un-petit-git-stash/).

### Comment on efface une branche distante ?

Comme Michel quelques pages plus tôt : `git push origin :branche_a_effacer`.

### C'est grave si je ne comprend pas tout ?

Pas du tout. Tout le monde n'est pas tenu de se passionner pour git, ni d'en comprendre les détails. `git checkout -b`, `git commit`, et `git push` sont le strict nécessaire pour que quelqu'un d'expérimenté y accède et vous aide à vous y retrouver.
