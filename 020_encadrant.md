# Encadrer un jeune développeur.

Prendre en main un nouveau développeur n'est pas aussi trivial qu'on pourrait le croire : il n'est pas évident de situer ses capacités de développement, il faut le mettre au fait des pratiques de l'équipe, s'assurer qu'il maîtrise les outils (a minima pour ne pas perturber ou parasiter le travail d'autrui)...

Tout ceci est vrai dans les équipes qui sont composées de développeurs pro "pur jus", et à *bien plus forte raison* dans une équipe de recherche où la compétence "programmer" n'est pas la principale.

## Qu'attendre d'un jeune développeur ?

Un étudiant ne peut --dans l'immense majorité des cas-- pas être un "bon" développeur. Maîtriser les syntaxes, les *design patterns*, les librairies et les bonnes pratiques prend du temps : ça s'apprend.

Aussi, il faut prendre en considération que l'acquisition de ces compétences est une nécessité pour être efficace en développement ; le développeur débutant ne le sera pas.

Il ne connaîtra pas des principes de programmation qui peuvent sembler évidents (don ton imagine que "tout le monde sait ça") ; il ne lira pas forcément facilement le code existant ; il n'aura pas la capacité de distinguer le cœur de la démarche de traitement des codes annexes plus accessoires (*e.g.* la préparation des données). Il ne saura pas séparer le Modèle, la Vue et le Contrôle, et par manque d'habitude et de pratique, sera (ou semblera) globalement assez lent dans ses tâches de programmation.

Il se peut que cela se ressente d'autant plus que les débutants se succèdent : alors que celui qui part a acquis une compétence, et une rapidité d'exécution, celui qui arrive semble, par contraste, d'autant moins performant. Ce n'est pas un état de fait immuable, il lui faut progresser, et pour cela il peut être utile de lui mettre le pied à l'étrier.

## Faciliter un démarrage réussi.

Le temps est la première ressource pour commencer à programmer. Il faut compter au minimum 600 h pour former un développeur débutant dans les formations professionnelles. Il s'agit alors de 600 h encadrées, suivies, appuyées de supports et d'exercices dans une démarche pédagogique. Des milliers d'heures passées seul sur un projet informatique ne permettront pas d'obtenir un résultat comparable.

Former
:   est fondamental. Si quelques dizaines d'heures passées à apprendre les bases du langage peuvent en faire gagner des centaines à l'équipe par la suite, l'investissement est plus que justifié.

Mettre en place un cadre de travail
:   facilite la prise de bonne habitudes de travail. S'assurer que le jeune développeur dispose de `git`, d'un analyseur de code[^linter], et d'un environnement de développement[^ide] adapté à la tâche, par exemple, permettra au jeune développeur de commencer plus efficacement à obtenir des résultats. Le respect de bonnes pratiques dès le début du travail lui évitera de ralentir le travail des autres développeurs, que leur travail sur le même projet soit simultané ou successif.

Suivre et accompagner la montée en compétence
:   (par exemple en s'assurant de la qualité du code, et de la démarche de programmation en plus de la justesse du résultat), permet de mieux adapter ses attentes au fil du travail, de mesurer et prendre en compte la compétence qui évolue, de corriger les infractions aux bonnes pratiques, etc. Toutes ces mesures n'ont qu'un but : s'assurer que le programme qui soutient ou démontre la démarche de recherche sera réutilisable et performant.

[^linter]: on parle plus souvent de *linter*
[^ide]: ou IDE pour *integrated development environment*
