# À propos de ce document.

Ce recueil n'est pas une référence de programmation. Il a pour objectif de permettre aux doctorants d'acquérir un peu plus rapidement les bonnes pratiques qui leur simplifieront le travail de programmation, et à leurs superviseurs, de mieux situer leurs attentes en comprenant mieux les tenants et aboutissants de la démarche de programmation.

La plupart des exemples de ce document seront en python, qui est relativement simple à lire, répandu en dans les communautés de recherche, et parce qu'il fallait bien choisir un langage.

Ce document est placé sous licence libre. Plus explicitement sous la licence ["WTFPL"](http://www.wtfpl.net)[^wtfpl]

[^wtfpl]: Do What the Fuck You Want to Public License

En premier lieu, rappelons quelques éléments important de contexte.

La place de la programmation dans la recherche:
:   Les sujets de recherche actuels sont toujours plus complexes. Il y a longtemps que les chercheurs s'intéressent à des domaines, des problèmes et des échelles qui ne sont plus accessibles sans instrumentation et sans traitements informatiques puissants.

    La programmation est devenue une nécessité pour continuer d'avancer sur la plupart des sujets. En contrepartie, les traitements informatiques nous procurent une précision et une reproductibilité essentielle pour la science.

    On[^on] attend donc des doctorants, et des étudiants en master, qu'ils contribuent à l'avancement des outils informatiques par leur travail.

[^on]: Dans le contexte de ce document, le pronom "on" se réfère presque toujours à "la communauté de recherche". Cela vaut pour pratiquement toutes les communautés, de la gestion industrielle à l'analyse des matériaux, à des degrés divers.

Le contexte de recherche:
:   L'informatique scientifique se caractérise par une grande complexité. Par corolaire, la seule description des tâches à accomplir est difficile à appréhender, en comparaison de l'informatique de gestion ou même de l'informatique embarquée. Cette complexité cause naturellement des programmes compliqués.

    Contrairement à l'image qu'en a le monde hors de la recherche, les délais de la recherche sont souvent assez court, au regard des tâches à accomplir
    Dans les quelques semaines de préparation d'une publication, le (jeune) chercheur doit non seulement apporter sa contribution sur le fond, et rédiger la publication en rendant compte, mais également construire le programme qui traitera sa proposition, le déboguer et tirer les conclusions de son traitement, par exemple sous la forme de figures[^figures_complexite].
    La combinaison de la grande complexité avec ces délais courts conduit souvent les développeurs en recherche à produire des code truffés de "bricolages" sur lesquels nous reviendrons plus loin, pour gagner un peu de temps avant la deadline, et qui en réalité compliquent encore un peu plus le travail par la suite.
    Quelques étudiants se "noient" dans leur travail suite à cette spirale, et gaspillent un temps précieux à leur efficacité comme à leur bien-être.

Enfin, la compétence des étudiants et jeunes chercheurs n'est pas informatique. Un spécialiste des matériaux, de la physique atomique, etc. n'est tout simplement pas un programmeur, ce n'est pas son métier. Il apprend à programmer, bon an, mal an, au fil de son travail de recherche. Il n'y a pas de raison particulière que cela le conduise à produire un code de qualité[^code_de_qualite].

[^figures_complexite]: Figures dont le rendu demande souvent un effort de travail sans commune mesure avec l'idée que le lecteur de la publication s'en fera.

[^code_de_qualite]: Le débat pourrait être très long sur ce qui distingue un code de qualité. Partons du principe qu'un code utilisé en recherche est retouché et modifié maintes fois, par différents chercheurs. Par conséquent, la qualité de ce code est de pouvoir passer de main en main facilement, c'est-à-dire d'être lisible, facilement modifiable, et bien documenté.

\mainmatter
