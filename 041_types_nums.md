## Types numériques de python

### NoneType, singleton de `None`

`None` est la valeur de "rien". Cette valeur spéciale représente l'absence de valeur. C'est aussi la valeur que renvoie une fonction qui ne donne pas de résultat. Son type est `NoneType`.

`NoneType` ne contient que la valeur `None` : c'est un type qui n'admet qu'un individu, un singleton.


### bool, type énuméré des valeurs logiques

La logique booléenne admet deux valeurs, "vrai" et "faux", souvent notées "1" et "0". Python les désigne sous les noms `True` et `False`.
Les opérateurs `or` et `and` correspondent aux opérations booléennes éponymes. `not` est la négation booléenne.
Le type `bool` de ces valeurs n'admet que celles-ci, on dit qu'il s'agit d'un type énuméré (cardinalité 2).

#### Évaluation des valeurs en contexte booléen
Tout objet en python a une valeur équivalente booléenne, qui vaut pour les tests logiques (la documentation officielle parle de *boolean context*).

  -------------------------------------------------------------
            `False`             `True`
  ---------------------------- --------------------------------
       nombre égal à zéro        tout autre nombre

   Séquence ou conteneur vide    tout autre conteneur

  -------------------------------------------------------------


### int, type des valeurs entières

Illimités en valeur, dans la limite de la mémoire disponible ($2^{31}$ ou $2^{64}$ dans d'autres langages).

 Opérateur    Opération
----------    ----------------------------------------------------------
`+`           addition
`-`           soustraction
`*`           multiplication


