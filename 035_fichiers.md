## Diviser pour mieux régner, (sur les fichiers sources)

Quelque méthode qui soit employée, la protection du travail accompli est fondamentale. Cette section présente les bases de la collaboration en développement, et les moyens de préserver son travail, c'est-à-dire une stratégie de sauvegarde basique.

### Répartir le code en modules

Les modifications apportées à un code seront bien plus facilement suivies par le logiciel de contrôle de version  (voir [la section correspondante sur `git`](#git)) si les modifications sont chacune assez petites en taille.

Par ailleurs le fichier qui utilise une portion de code n'a pas besoin de contenir cette portion de code : par exemple, le code de `count_divisors` n'est pas nécessairement inclus dans le même fichier que ses appels.

~~~{.python .numberLines}
def count_divisors(x: "number to divide"):
    """
    returns number of divisors for numbers > 1
    """

    counter = 2
    for i in range(2, int(x ** 0.5 + 1)):
        if x % i == 0:
            counter += 2
    return counter
~~~

Le code peut être très clair (pour peu que la fonction porte un nom explicite, et que sa *docstring* soit renseignée) avec dans un second fichier la ligne `form mymath.number_theory import count_divisors`.

Il existe trois structures importables en python : les packages (et leurs sous paquets), les modules et les objets eux-mêmes.

Dans la forme  `from mymath.number_theory import count_divisors`, `mymath` est un package, qui contient un module `number_theory`, dans lequel est définie une fonction^[puisque son identifiant ne contient pas de majuscules !] `count_divisors`.

Encore faut il que ce chemin d'`import` soit logique et s'utilise donc naturellement... Et pour cela il convient de respecter une logique (au sein du projet) pour nommer les modules et les objets qu'ils contiennent^[de cette façon, l'auto complétion devient *vraiment* très rapide pour appeler des imports "lointains".].

La façon de structurer les *packages* (rassemblements de modules) dépasse le cadre de cette section, et est abordée dans le chapitre [presentant les bases de python](#packages-modules)

Un exemple de logique efficace pour nommer et organiser les modules est le suivant :

-   les *packages* et les *modules* de premier niveau représentent les domaines, et sont indépendants de la solution d'implémentation. Par exemple : `optique`, `analyse_graphique`, `visualisation`, `data_io`^[pour "data import and export" ou "data-in-out"], `misc`, etc.
-   les *modules* et *packages* de second niveau concernent une spécialité. Par exemple, on pourra utiliser `optique.deconvolution`, `optique.parametrage`, `visualisation.plot`, ou `data_io.archive` par exemple.
-   les *fonctions* et *classes* représentent des actions (des fonctions) ou des objets (pour les classes), et sont "atomiques", c'est-à-dire qu'elles doivent pouvoir être importées individuellement. Plus précisément leur usage dans un code ne doit pas imposer à ce code de se servir d'autres imports en particulier. Par exemple, on pourra utiliser les objets suivants : `optique.parametrage.estimer_parametres()`, ou `data_io.archive.write_zip()`, dont les noms seront assez explicites pour rendre le code lisible.

En utilisant une logique de cet ordre, il devient réellement utile d'importer les modules "complets"^[c'est à dire par `import monmodule`], et d'écrire dans le corps du programme des expressions certes plus longues, mais nettement plus explicites comme `fuzzy.set.operations.complement` ou `data_io.import.data_from_store()`. Notons d'ailleurs la difficulté de lecture du code qui serait causée par l'usage de `from data_io import *`, le contexte donné par le nom du paquet est dans ce cas perdu dans le code.

En ce qui concerne la taille des modules (combien de lignes contient chaque fichier), les politiques varient d'un projet à l'autre, d'une entreprise à l'autre, voire d'un groupe de travail à l'autre. Les langages peuvent avoir leurs exigences également : Java par exemple impose d'avoir une seule classe déclarée par fichier.

Il est intéressant de rester "raisonnable" en taille de fichiers, et de ne pas dépasser quelques centaines de lignes. Un plus grand fichier est plus difficile, voire impossible à maintenir, et à modifier. Cette question, qui rejoint celle de la complexité, est traitée plus en détails dans la [partie sur les bonnes pratiques](#complexite).

### Sauvegarder les fichiers et les versions

Selon la qualité du code produit, il sera plus ou moins lisible et maintenable. Il sera de toutes façon nécessaire de sauvegarder le cadre de travail pour parer aux aléas divers^[On peut penser à la panne, la fausse manipulation, qui détruisent les fichiers, mais l'expérience montre que la plupart des cas où il faut revenir à une version antérieure sont en fait des "égarements" d'un project qui dérive d'un cahier des charges au fil du temps].

Il n'est nécessaire de conserver que les données à traiter, quoique elles ne font pas à proprement partie du code, et les fichiers sources.
Les fichiers intermédiaires, les binaires compilées, les caches, etc. n'ont aucune raison d'être conservés avec le code source du projet, puisque ils seront régénérés à l'exécution du programme.

Idéalement, chaque modification de chaque fichier devrait être sauvegardée individuellement : on pourra revenir à n'importe quelle version de n'importe quel fichier en cas de besoin.

Cela étant, pour que ces modification individuelles ne corrompent pas l'exécution du programme, il est impératif que le code soit aussi indépendant que possible de lui-même. On veut donc éviter le "spaghetti code", ou chaque modification aurait des répercutions à compenser en plusieurs endroits du code.

`git` est un outil de gestion des versions et de travail distribué qui donne les moyens de travailler selon ces exigences, il est présenté dans la partie [qui lui est consacrée de ce document](#git) .
