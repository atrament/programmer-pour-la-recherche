# Un exemple de "Workflow" sûr avec git

L'outil "`git`" lui-même n'est pas présenté dans cette section, pour ne pas encombrer la réflexion d'aspects techniques inutiles. Pour le moment, considérons simplement qu'il tient un journal des modifications du dossier courant, et que chacune de ses entrées, appelée *commit*, est référence l'état précédent du dossier.

`git` construit donc une sorte d'"arbre" des modifications où chaque "branche" peut évoluer indépendamment des autres (et donc ou chacun peut se concentrer sur son travail sans risquer de perturber ses confrères et réciproquement).

## Organisation des fichiers et du code

### Fichiers sources

#### Fichier principal

Par convention, le fichier qui devrait être lancé se nomme `main` (ou `main.py` en python). On peut aussi, pour les systèmes Unix ou Linux qui disposent de `make`, écrire un `Makefile` pour lancer les scripts.

    # Makefile de projet python
    # Projet Analyse_du_bidule

    run:
        python3 main.py

De la sorte, les utilisateurs sous Linux pourront simplement taper "`make run`" pour lancer le projet comme il doit l'être.

#### Répertoires

Les données devraient être installées dans un sous-répertoire adapté, comme "`data`", et suivre une convention de nommage. Cette convention de nommage peut être indiquée dans un fichier "`README`" (en texte brut, markdown, mais --à tout prix-- lisible avec un éditeur de texte) à la racine du projet.

Chaque outil rédigé en python est ensuite préparé sous la forme d'un "`package`", de sorte à occuper un répertoire individuel.^[sans entrer dans plus de détails sur les packages, il y aura donc toujours un fichier `__init__.py` dans ces dossiers.]

La structure du dossier pourrait donc ressembler à ceci :

    projet_bidule/
    |-- data
    |   |-- exp_laser_marseille
    |   |   |-- 2015-06-22_laser_bluelight_rawdata.dat
    |   |   `-- 2015-06-22_laser_redshift_matricedeconvolution.mat
    |   `-- exp_synchro_xray
    |       |-- 2015-12-06_xray_lowpower_rawdata.dat
    |       `-- 2015-12-22_xray_5grey_rawdata.dat
    |-- doc
    |   |-- api.txt
    |   |-- contributing.txt
    |   |-- install.txt
    |   |-- quickstart.txt
    |   `-- usage.txt
    |-- inc
    |   |-- article_001.pdf
    |   |-- article_002.pdf
    |   |-- article_003.pdf
    |   `-- README.txt
    |-- main.py
    |-- raynalyse
    |   |-- __init__.py
    |   |-- optoconvert.py
    |   `-- vectools.py
    `-- README.md

Dans cet exemple, la documentation du code lui-même se trouve dans le dossier `doc`, où `api.txt` donne les détails de l'usage des capacités du programme dans un autre (les fonctions à importer, etc.), `install.txt` les instructions pour configurer la machine afin d'exécuter le programme, `usage.txt` les instructions d'utilisation détaillées, `quickstart.txt` prodigue les instruction pour mettre le pied à l'étrier rapidement, et `contributing.txt` présente les particularités du workflow employé, afin que les contributions soient gérées au mieux.

Cet exemple comprend également un dossier "`inc`"^[qui pourrait aussi s'appeler `info` ou `etc`...], lequel contient des articles de recherche qui présentent les fondements de l'outil informatique. En centralisant les informations les plus pertinentes au sein du projet lui-même, le nouvel arrivant dans le projet aura de suite accès à tout le nécessaire pour s'informer selon ses besoins (et ceux de sa mission au sein du projet).

Les fichiers de programme du projet sont dans le dossier "`raynalyse`"^[ce projet imaginaire doit avoir un lien avec une analyse par rayonnement], qui est un *package* python, comme l'indique l'existence de ` __init__.py`.

Enfin, les données (de test, mais aussi d'exploitation, le cas échéant) fournies avec le projet se trouvent dans le dossier "`data`", où elles sont groupées par séries selon leur provenance, selon une convention de nommage du type `<type>_<tech>_<mnemonique>` où `<type>` serait `exp` pour les expérimentation, `simu` pour les données simulées, etc. ; `<tech>` donne une information pertinente sur la technique employée, et le mnémonique est libre pour identifier les données plus précisément.

Au sein de ces dossiers de données, les fichiers sont individuellement nommées sur la forme `<date_iso>_<tech>_<mnemonique>_<nature>.<ext>`. Le type du fichier est normalement donnée par son extension, la `<nature>` permet donc d'identifier plus explicitement les données contenues dans le fichier. La "matrice de convolution" est ici immédiatement identifiable.

### Structure du code en blocs indépendants

Une structure de dossiers cohérente doit permettre de se retrouver au sein du projet, et de ne pas avoir de doute quand il s'agira d'ajouter des données ou du code à ce projet.
Mais le code proprement dit doit être structuré pour éviter les complexités et circonvolutions inutiles qui conduisent au *spaghetti code*.

On tâchera donc de construire des fonctions courtes, d'usage le plus général possible mais pour une fonction précise. En particulier, une fonction ne doit jamais être écrite pour un jeu de données spécifique : "`calibrage_machine_monaco`" est une fonction soit trop spécifique soit très mal nommée ! On peut imaginer :

-   faire évoluer la fonction de calibrage générale pour accepter les paramètres de la machine, ou mieux encore, de n'importe quelle machine.
-   réécrire cette fonction pour une famille de machine et la nommer plutôt "`calibrage_mmt_femto`" pour refléter son rôle désormais moins restreint.
-   inclure cette fonction dans une classe consacrée au calibrage.
-   associer des méta données de calibrage aux données à traiter.
-   et encore de nombreuses autres solutions...

## arbre des modifications


### branches

-   La branche principale "`master`"
-   Une branche de support pour les développements "`dev`" ou "`develop`"
-   Une branche "éternelle"^[*long-lived*] par sujet majeur (par exemple "`modele`", "`interface`", "`test_data`", etc.), qui sont fusionnées vers "`dev`" quand elles sont matures.
-   Autant de branches "éphémères" que nécessaire pour les ajouts et les interventions. Ces branches ont typiquement une durée de vie de quelques heures ou quelques jours, et sont fusionnées à "`dev`" où à leur branche "éternelle" mère dès que la tâche à accomplir est effectuée.

![Arbre des *commits* au début du projet](img/workflow_git_1.pdf){#fig:workflow01}

Au début du projet (@fig:workflow01), trois branches sont créées pour le modèle, les données, et l'interface. Le travail sur chacun de ces sujets est donc disjoint et indépendant des autres branches.

Sur la première figure, après quelques jours de travail, une tâche a été implémentée pour l'interface comme pour les données, et deux sur le modèle. Le troisième et le sixième *commit* de la branche modèle montrent en effet la fusion des branches éphémères dans la branche consacrée au modèle. Dans un dépôt git, les messages de ces commits seraient d'ailleurs "*merge <branche> into <branche>*", rendant cette distinction d'autant plus explicite.

![Arbre des *commits* après quelques jours de travaux](img/workflow_git_2.pdf){#fig:workflow02}

À la figure suivante (@fig:workflow02), le travail a continué sur le modèle, mais cette jeune branche n'est pas encore terminée (en jaune plus vif). Elle n'est donc pas fusionnée à nouveau dans la branche principale.

Les données on par contre été estimées utilisables pour les démonstrations ou les tests, et donnent lieu à une première fusion vers la branche "dev". De même, le travail vérifié de la branche du modèle est également fusionné à la branche "dev".

L'interface aura demandé plus de travail avant sa première fusion vers "`dev`" : après 3 fusions de branches éphémères, elle est enfin fusionnée au reste du projet dans dev.

Les tests et corrections sont faites sur une branche éphémère de "`dev`" (ces commits sont indiqués par une étoile). Ces tests et corrections sont ensuite répercutées vers les autres branches qui récupèrent les évolutions de "`dev`" quand elles sont prêtes à les intégrer (fusions en rouge, sur @fig:workflow03)

![récupération des évolutions de "`dev`" dans les branches de sujet](img/workflow_git_3.pdf){#fig:workflow03}

L'évolution du code peut continuer à partir de là, ces branches les plus récentes étant à jour des modifications des autres branches.

Imaginons que le projet soit estimé suffisamment mature ou stable pour donner lieu à une *release*, on créera une fusion de `dev` vers `master` (avec quelques tests, et commits complémentaires au besoin) pour mettre à disposition cette version stable dans la branche "`master`". Cette dernière possibilité est illustrée sur @fig:workflow04.

![Première version stable du projet dans la branche "master"](img/workflow_git_4.pdf){#fig:workflow04}

Ce mode de travail a l'avantage de n'imposer une synchronisation des différents sujet de travail que lors des fusion de branches, après laquelle chacun peut reprendre son travail indépendamment des autres branches. L'arbre des *commits* est également très lisible, chaque "mission" pour faire évoluer le code est repérable par la branche éphémère qui lui correspond.

On peut bien sût choisir d'autres *workflows* avec git, comme le *github workflow*, le *gitlab workflow*, ou le *git flow* (il existe pléthore de propositions, chacune avec ses atouts et ses inconvénients).

Cependant, le point essentiel est de convenir d'un workflow commun, de le communiquer et le documenter ("`contributing.txt`" serait parfaitement adapté pour cela) et de s'y tenir, de façon à ce que tous les contributeurs travaillent confortablement, et donc plus efficacement.

Enfin, et en forme de réserve : Il n'est pas difficile, ni rare, de construire avec git^[certains disent "à cause de git"] des arbres très complexes de modifications, absolument illisibles. Un peu de rigueur est nécessaire pour éviter les mauvaises surprises.
