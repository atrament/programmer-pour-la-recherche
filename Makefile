IMGDIR := img

IMGS := $(wildcard $(IMGDIR)/*.png)

DOTS := $(subst .dot,.pdf,$(wildcard $(IMGDIR)/*.dot))

SVG := $(subst .svg,.pdf,$(wildcard $(IMGDIR)/*.svg))

MDS := meta.yaml $(sort $(wildcard *.md))

TARGET := book.pdf

# Phony targets
.PHONY : clean all edit help new

$(TARGET): $(MDS) $(IMGS) $(DOTS) $(SVG)
	pandoc --smart \
		--latex-engine=xelatex \
		--filter pandoc-fignos \
		--filter pandoc-citeproc \
		--number-sections \
		-o $@ $(MDS)

bump:
	vim +'norm 4ggg_^dW:r! date -I"ddg_ddk^"dPa ' meta.yaml

help:
	@echo "COMMANDS:"
	@echo "                 Build the PDF taget"
	@echo "  book.html      Build the HTML targets"
	@echo ""
	@echo " bump			bump the version number and edit meta"
	@echo "  help           Show this text"
	@echo "  clean          Remove all generated files. (DOT)"
	@echo "  edit           Launch GVIM with all source files"
	@echo "  new            Make a new md file for today and/or edit it"
	@echo "  view           See the PDF in atril"

clean:
	rm -f $(DOTS) $(SVG) $(TARGET)

edit:
	gvim +'nnoremap <F1> :wa<CR>:!make<CR>'  Makefile $(MDS)

new:
	test -f $(shell date -I).md || echo "# $(shell date -I)" > $(shell date -I).md

view: $(TARGET)
	atril $(TARGET) 1>&2 2> /dev/null &

#add 'N' for numbered chapters
#
# concrete targets

# book.html: $(MDS) $(IMGS) $(DOTS)
# 	pandoc --smart --latex-engine=xelatex --latexmathml --standalone --filter pandoc-citeproc -o $@ $(MDS)

# Pattern rules


%.tex : %.md
	pandoc --smart --latex-engine=xelatex --filter pandoc-citeproc -No $@ $<

%.pdf : %.svg
	inkscape --export-pdf=$@ $<

%.pdf : %.md
	pandoc --smart --latex-engine=xelatex --filter pandoc-citeproc -No $@ $<

%.pdf : %.dot
	dot -Tpdf -Gmargin="0"  $< > $@

%.png : %.dot
	dot -Tpng $< > $@

$(IMGS): | $(IMGDIR)

$(IMGDIR):
	mkdir $(IMGDIR)

%:
	touch $@
